package com.cuupa.mailprocessor.services;

import com.cuupa.mailprocessor.configuration.application.MappingProperties;

import java.util.Locale;

public class Mappings {

	public static String getMapping(String topic, String user, Locale locale) {
		 MappingProperties mappingProperties = new MappingProperties(user);
		return mappingProperties.get(topic, user, locale);
	}
}
