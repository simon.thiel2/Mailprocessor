package com.cuupa.mailprocessor.services;

import com.cuupa.mailprocessor.configuration.application.ApplicationProperties;
import com.cuupa.mailprocessor.configuration.application.MailProperties;
import com.cuupa.mailprocessor.configuration.application.ScanProperties;
import com.cuupa.mailprocessor.services.input.email.MailService;
import com.cuupa.mailprocessor.services.input.scan.ScanService;
import com.cuupa.mailprocessor.tos.mail.email.EMail;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

public class WorkerService {

    private final Log LOG = LogFactory.getLog(WorkerService.class);

    private final ProcessService processService;

    private final ScanService scanService;

    private final MailService mailService;

    public WorkerService(ProcessService processService, MailService mailService, ScanService scanService) {
        this.processService = processService;
        this.mailService = mailService;
        this.scanService = scanService;
    }

    @Scheduled(cron = "0 */30 * * * *")
    public void executeMail() {
        try {
            LOG.debug("... starting executeMail");

            for (String user : ApplicationProperties.getUsers()) {
                List<String> labels = new MailProperties(user).getLabels();

                for (String label : labels) {
                    try {
                        List<EMail> emails = mailService.loadMails(label, user);
                        processService.startProcess(emails);
                    } catch (Exception e) {
                        LOG.fatal("Failed to execute worker", e);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0 * * * * *")
    public void executeScan() {
        try {
            LOG.debug("... starting executeScan");

            for (String user : ApplicationProperties.getUsers()) {
                String folder = new ScanProperties(user).getFolder();
                processService.startProcess(scanService.loadScans(folder, user));
            }
        } catch (Exception e) {
            LOG.error("Failed to execute Scan-Task", e);
        }
    }
}
