package com.cuupa.mailprocessor.services;

import com.cuupa.mailprocessor.configuration.application.ApplicationProperties;
import com.cuupa.mailprocessor.configuration.application.ReminderProperties;
import com.cuupa.mailprocessor.process.MailInstanceHandler;
import com.cuupa.mailprocessor.tos.semantic.Metadata;
import com.rometools.utils.Strings;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class ReminderDelegate implements JavaDelegate {

    private final Log logger = LogFactory.getLog(ReminderDelegate.class);

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        MailInstanceHandler handler = new MailInstanceHandler(execution);

        ReminderProperties properties = new ReminderProperties(handler.getUser());
        if (!properties.hasReminder()) {
            return;
        }

        final List<Metadata> metaData = handler.getMetaData();
        final String dueDate = getDueDate(metaData);
        DateTimeFormatter parse = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        if (Strings.isBlank(dueDate) || LocalDate.parse(dueDate, parse).isBefore(LocalDate.now())) {
            return;
        }

        File folderForReminder = createReminderFolder(handler);
        File fileForReminder = new File(folderForReminder, dueDate);

        String text = getTextForReminder(handler, metaData, dueDate);
        String urlToCallWithValue = properties.getUrlToCall() + URLEncoder.encode(text, StandardCharsets.UTF_8.name());
        Files.write(fileForReminder.toPath(), urlToCallWithValue.getBytes(StandardCharsets.UTF_8));
        callReminder(urlToCallWithValue);
    }

    private File createReminderFolder(final MailInstanceHandler handler) {
        File folderForReminder = new File(ApplicationProperties.getConfigurationDir() + handler.getUser()
                                                  + ApplicationProperties.getFileSeperator() + "reminder");
        folderForReminder.mkdirs();
        return folderForReminder;
    }

    private boolean callReminder(final String urlToCallWithValue) {
        try {
            URL url = new URL(urlToCallWithValue);
            URLConnection openConnection = url.openConnection();
            StringBuilder sb = new StringBuilder();
            InputStream is = new BufferedInputStream(openConnection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            String response = sb.toString();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @NotNull
    private String getTextForReminder(MailInstanceHandler handler, List<Metadata> metaData, String dueDate) {
        return "Das Schreiben \"" + getStringForTopics(handler) + handler.getFileName() + "\" von \"" + handler.getFrom()
                + "\" erfordert bis zum " + dueDate + " ihre Aufmerksamkeit.\n"
                + metaData.stream().map(m -> m.toString() + "\n").collect(Collectors.joining());
    }

    private String getStringForTopics(MailInstanceHandler handler) {
        return handler.getTopics().stream().map(e -> Mappings.getMapping(e, handler.getUser(), Locale.GERMAN) + "_")
                      .collect(Collectors.joining());
    }

    private String getDueDate(List<Metadata> metaData) {
        String dueDate = "";
        for (Metadata data : metaData) {
            if ("dueDate".equals(data.getName())) {
                dueDate = data.getValue();
            }
        }
        return dueDate;
    }

    public synchronized void executeReminder(String parameter) {
        callReminder(parameter);
    }

    public boolean isCandidateForReminder(String filename) {
        if (StringUtils.isBlank(filename) || !filename.contains(".")) {
            return false;
        }

        LocalDate localDate = LocalDate.parse(filename, formatter);
        if (LocalDate.now().isAfter(localDate)) {
            return false;
        }

        if (localDate.minusDays(1).isEqual(LocalDate.now())) {
            return true;
        }

        return localDate.minusDays(7).isEqual(LocalDate.now());
    }

    public boolean isReminderToRemove(String filename) {
        if (StringUtils.isBlank(filename) || !filename.contains(".")) {
            return false;
        }

        LocalDate localDate = LocalDate.parse(filename, formatter);
        return LocalDate.now().isAfter(localDate);

    }

    public void removeReminder(File file) {
        try {
            Files.delete(file.toPath());
        } catch (IOException e) {
            logger.error("Failed to remove reminder " + file.getName(), e);
        }
    }
}
