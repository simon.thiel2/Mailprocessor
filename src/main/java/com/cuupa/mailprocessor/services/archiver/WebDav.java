package com.cuupa.mailprocessor.services.archiver;

import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

public class WebDav implements FileProtocol {

	private final Sardine sardine;

	public WebDav(String username, String password) {
		sardine = SardineFactory.begin(username, password);
		sardine.enableCompression();
	}

	@Override
    public void close() throws IOException {
		sardine.shutdown();
	}

    @Override
    public boolean exists(String path) {
		try {
			return sardine.exists(path);
        } catch (IOException e) {
			return true;
		}
	}

    @Override
    public boolean save(String path, byte[] data) {
        try {
            sardine.put(path, data);
            return true;
        } catch (IOException e) {
            return false;
        }
	}

    @Override
    public void createDirectory(String directory) throws IOException {
        sardine.createDirectory(directory);
    }

    @Override
    public List<ArchiveResource> list(String url) throws IOException {
        return sardine.list(url, 1).stream().map(ArchiveResource::new).collect(Collectors.toList());
    }

    @Override
    public InputStream get(String url) throws IOException {
        return sardine.get(url);
    }

    @Override
    public void delete(String url) throws IOException {
        sardine.delete(url);
    }
}
