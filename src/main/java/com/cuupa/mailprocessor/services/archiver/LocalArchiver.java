package com.cuupa.mailprocessor.services.archiver;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class LocalArchiver implements FileProtocol {

    @Override
    public boolean exists(String path) {
        return Files.exists(Paths.get(path));
    }

    @Override
    public boolean save(String path, byte[] data) {
        new ByteArrayOutputStream();
        try {
            Files.copy(new ByteArrayInputStream(data), Paths.get(path));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void createDirectory(String directory) throws IOException {
        Files.createDirectory(Paths.get(directory));
    }

    @Override
    public List<ArchiveResource> list(String url) throws IOException {
        return Files.list(Paths.get(url)).map(Path::toFile).map(ArchiveResource::new).collect(Collectors.toList());
    }

    @Override
    public InputStream get(String url) throws IOException {
        return Files.newInputStream(Paths.get(url));
    }

    @Override
    public void delete(String url) throws IOException {
        Files.delete(Paths.get(url));
    }

    @Override
    public void close() throws IOException {

    }
}
