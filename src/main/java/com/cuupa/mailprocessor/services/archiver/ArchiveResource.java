package com.cuupa.mailprocessor.services.archiver;

import com.github.sardine.DavResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ArchiveResource {

    private DavResource davResource;

    private File file;

    ArchiveResource(DavResource davResource) {
        this.davResource = davResource;
    }

    ArchiveResource(File file) {
        this.file = file;
    }

    public boolean isPdf() {
        if (davResource != null) {
            return "application/pdf".equals(davResource.getContentType());
        } else if (file != null) {
            try {
                return "application/pdf".equalsIgnoreCase(Files.probeContentType(file.toPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public String getName() {
        if (davResource != null) {
            return davResource.getName();
        } else if (file != null) {
            return file.getName();
        }
        return null;
    }
}
