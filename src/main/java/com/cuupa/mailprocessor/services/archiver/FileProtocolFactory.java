package com.cuupa.mailprocessor.services.archiver;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

public class FileProtocolFactory {

    private static final Pattern patternWindows = Pattern.compile("([A-Z]):\\\\[a-zA-Z0-9\\.\\\\]*");

    private static final Pattern patternUnix = Pattern.compile("/[a-zA-Z0-9/]*");

    public static FileProtocol get(final String address, final String username, final String password) {
        if (StringUtils.isBlank(address)) {
            return null;
        }

        if (isWebDav(address)) {
            return new WebDav(username, password);
        } else if (isLocal(address)) {
            return new LocalArchiver();
        }

        return null;
    }

    public static boolean isLocal(String address) {
        return patternWindows.matcher(address.replace("/", "\\")).matches() || patternUnix.matcher(address).matches();
    }

    public static boolean isWebDav(String address) {
        return address.startsWith("http://") || address.startsWith("https://");
    }
}
