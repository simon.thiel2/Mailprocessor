package com.cuupa.mailprocessor.services.archiver;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface FileProtocol extends AutoCloseable {

    boolean exists(String path);

    boolean save(String path, byte[] data);

    void createDirectory(String directory) throws IOException;

    List<ArchiveResource> list(String url) throws IOException;

    InputStream get(String url) throws IOException;

    void delete(String url) throws IOException;
}
