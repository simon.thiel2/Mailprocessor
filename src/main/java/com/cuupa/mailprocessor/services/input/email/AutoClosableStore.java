package com.cuupa.mailprocessor.services.input.email;

import javax.mail.MessagingException;
import javax.mail.Store;

/**
 * @author Simon Thiel (https://github.com/cuupa)
 */
public class AutoClosableStore implements AutoCloseable {

    private final Store store;

    protected AutoClosableStore(Store store) {
        this.store = store;
    }

    @Override
    public void close() throws MessagingException {
        store.close();
    }

    public Store getStore() {
        return store;
    }
}
