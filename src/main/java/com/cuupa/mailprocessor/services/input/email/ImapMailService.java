package com.cuupa.mailprocessor.services.input.email;

import com.cuupa.mailprocessor.configuration.application.MailProperties;
import com.cuupa.mailprocessor.tos.mail.email.EMail;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ImapMailService implements MailService {

    private static final int chunkSize = 500;

    private final Log LOG = LogFactory.getLog(ImapMailService.class);

    private final FetchProfile fetchProfile;

    public ImapMailService() {
        fetchProfile = new FetchProfile();
        fetchProfile.add(FetchProfile.Item.FLAGS);
        fetchProfile.add(FetchProfile.Item.ENVELOPE);
    }

    @Override
    public List<EMail> loadMails(String label, String user) throws Exception {
        try (AutoClosableStore store = getStoreAndConnect(user);
             AutoClosableIMAPFolder folder = new AutoClosableIMAPFolder(store.getStore().getFolder(label))) {
            folder.openFolder(Folder.READ_ONLY);
            List<EMail> messageList = new ArrayList<>(loadMessages(folder, user));
            return messageList;

        }
    }

    @Override
    public void markMailAsRead(EMail mail) throws Exception {
        try (AutoClosableStore store = getStoreAndConnect(mail.getUser());
             AutoClosableIMAPFolder folder = new AutoClosableIMAPFolder(store.getStore()
                                                                             .getFolder(mail.getLabel()))) {
            folder.openFolder(Folder.READ_WRITE);
            markMailAsRead(mail, folder);
        }
    }

    private void markMailAsRead(EMail mail, AutoClosableIMAPFolder folder) throws MessagingException {
        long largestUid = folder.getUIDNext() - 1;
        for (long offset = 0; offset < largestUid; offset += chunkSize) {
            Message[] messages = getMessages(folder, largestUid, offset);

            folder.fetch(messages, fetchProfile);

            for (int i = messages.length - 1; i >= 0; i--) {
                Message message = messages[i];

                if (isSubjectEquals(mail, message) && isReceivedDateEquals(mail, message)) {
                    message.setFlag(Flags.Flag.SEEN, true);
                    break;
                }
            }
        }
    }

    private boolean isReceivedDateEquals(EMail mail, Message message) throws MessagingException {
        return message.getReceivedDate().equals(mail.getReceivedDate());
    }

    private boolean isSubjectEquals(EMail mail, Message message) throws MessagingException {
        if (message.getSubject() == null && mail.getSubject() == null) {
            return true;
        } else if (message.getSubject() == null && mail.getSubject() != null) {
            return false;
        } else if (message.getSubject() != null && mail.getSubject() == null) {
            return false;
        }

        return message.getSubject().equals(mail.getSubject());
    }

    private Message[] getMessages(AutoClosableIMAPFolder folder, long largestUid, long offset) throws MessagingException {
        long start = Math.max(1, largestUid - offset - chunkSize + 1);
        long end = Math.max(1, largestUid - offset);
        return folder.getMessagesByUID(start, end);
    }

    private List<EMail> loadMessages(AutoClosableIMAPFolder folder, String user)
            throws MessagingException, IOException {
        long start = System.currentTimeMillis();
        List<EMail> messageList = new ArrayList<>();
        long largestUid = folder.getUIDNext() - 1;
        for (long offset = 0; offset < largestUid; offset += chunkSize) {
            Message[] messages = getMessages(folder, largestUid, offset);

            folder.fetch(messages, fetchProfile);

            for (int i = messages.length - 1; i >= 0; i--) {
                Message message = messages[i];
                boolean isRead = message.isSet(Flags.Flag.SEEN);

                if (!isRead) {
                    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                        message.writeTo(outputStream);

                        EMail email = new EMail();
                        email.setSubject(message.getSubject());
                        email.setLabel(folder.getFullName());
                        email.setReceivedDate(message.getReceivedDate());
                        email.setContent(outputStream.toByteArray());
                        email.setUser(user);

                        Address[] sender = message.getFrom();
                        String emailAddress = sender == null ? null : ((InternetAddress) sender[0]).getAddress();
                        email.setFrom(emailAddress);

                        messageList.add(email);
                    }
                }
            }
        }
        long end = System.currentTimeMillis();
        LOG.debug("DAUERTE " + (end - start) + "ms");
        return messageList;
    }

    private AutoClosableStore getStoreAndConnect(String user) throws MessagingException {
        MailProperties props = new MailProperties(user);
        Store store = getStore(props);
        store.connect(props.getServername(), props.getUsername(), props.getPassword());
        return new AutoClosableStore(store);
    }

    private Store getStore(MailProperties props2) throws NoSuchProviderException {
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");
        props.setProperty("mail.imaps.host", props2.getServername());
        props.setProperty("mail.imaps.port", "993");
        props.setProperty("mail.imaps.connectiontimeout", "5000");
        props.setProperty("mail.imaps.timeout", "5000");
        Session session = Session.getDefaultInstance(props, null);
        return session.getStore("imaps");
    }
}
