package com.cuupa.mailprocessor.services.input.email;


import com.sun.mail.imap.IMAPFolder;

import javax.mail.FetchProfile;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;

/**
 * @author Simon Thiel (https://github.com/cuupa)
 */
public class AutoClosableIMAPFolder implements AutoCloseable {

    private final Folder folder;

    public AutoClosableIMAPFolder(Folder folder) {
        this.folder = folder;
    }

    @Override
    public void close() throws Exception {
        folder.close(false);
    }

    public Folder getFolder() {
        return folder;
    }

    public long getUIDNext() throws MessagingException {
        if (folder instanceof IMAPFolder) {
            return ((IMAPFolder) folder).getUIDNext();
        }

        return 0L;
    }

    public void fetch(Message[] messages, FetchProfile fetchProfile) throws MessagingException {
        folder.fetch(messages, fetchProfile);
    }

    public boolean isOpen() {
        return folder.isOpen();
    }

    public void open(int state) throws MessagingException {
        folder.open(state);
    }

    public Message[] getMessagesByUID(long start, long end) throws MessagingException {
        if (folder instanceof IMAPFolder) {
            return ((IMAPFolder) folder).getMessagesByUID(start, end);
        } else {
            return new Message[0];
        }
    }

    public String getFullName() {
        return folder.getFullName();
    }

    public void openFolder(int state) {
        if ((folder instanceof IMAPFolder)) {
            try {
                if (!folder.isOpen()) {
                    folder.open(state);
                }
            } catch (MessagingException mse) {
            }
        }
    }
}
