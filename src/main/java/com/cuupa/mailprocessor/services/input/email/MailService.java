package com.cuupa.mailprocessor.services.input.email;

import com.cuupa.mailprocessor.tos.mail.email.EMail;

import java.util.List;

public interface MailService {

	List<EMail> loadMails(String label, String user) throws Exception;

	void markMailAsRead(EMail mail) throws Exception;

}
