package com.cuupa.mailprocessor.services.input.scan;

import com.cuupa.mailprocessor.configuration.application.ScanProperties;
import com.cuupa.mailprocessor.services.archiver.ArchiveResource;
import com.cuupa.mailprocessor.services.archiver.FileProtocol;
import com.cuupa.mailprocessor.services.archiver.FileProtocolFactory;
import com.cuupa.mailprocessor.tos.mail.PDF;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ScanService {

    public Collection<PDF> loadScans(String folder, String user) {
        ScanProperties scanProperties = new ScanProperties(user);
        final String url = getUrl(scanProperties, folder).toString();

        try (FileProtocol fileProtocol = FileProtocolFactory.get(url,
                                                                 scanProperties.getUsername(),
                                                                 scanProperties.getPassword())) {
            return fileProtocol.list(url)
                               .stream()
                               .filter(ArchiveResource::isPdf)
                               .filter(e -> isCorrectScanner(e, scanProperties))
                               .map(e -> createPdfObject(user, fileProtocol, url, e))
                               .collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    private boolean isCorrectScanner(ArchiveResource resource, ScanProperties scanProperties) {
        return scanProperties.getScannerPrefix().stream().anyMatch(prefix -> resource.getName().startsWith(prefix));
    }

    public List<PDF> loadScanIntern() {
        List<PDF> value = new ArrayList<>();
        PDF pdf = new PDF();
        File file = new File(
                "C:\\Users\\Simon\\SynologyDrive\\Desktop\\Testdaten\\BRN3C2AF4402B24_20190729_191011_001648.pdf");
        try (InputStream in = new FileInputStream(file)) {
            pdf.setContent(IOUtils.toByteArray(in));
            pdf.setFileName(file.getName());
            pdf.setUser("simon.thiel");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        value.add(pdf);
        return value;
    }

    private PDF createPdfObject(String user, FileProtocol fileProtocol, final String url, ArchiveResource resource) {
        PDF pdf = new PDF();
        try (InputStream inputStream = fileProtocol.get(url + resource.getName().replace(" ", "%20"))) {
            pdf.setContent(IOUtils.toByteArray(inputStream));
            pdf.setFileName(resource.getName());
            pdf.setUser(user);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return pdf;
    }

    private URL getUrl(ScanProperties properties, String path) {
        if (FileProtocolFactory.isLocal(properties.getAddress())) {
            try {
                return new File(properties.getAddress()).toURI().toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        try {
            return new URI(getScheme(properties.getAddress()),
                           null,
                           getHost(properties.getAddress()),
                           getPort(properties.getAddress()),
                           "/" + path + "/",
                           null,
                           null).toURL();
        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }

    private int getPort(String address) {
        if (address.contains(":")) {
            String[] addressArray = address.split(":");
            String port = addressArray[addressArray.length - 1];
            return Integer.parseInt(port);
        }
        return -1;
    }

    private String getHost(String address) {
        String addressWithoutScheme = address.split("://")[1];
        if (addressWithoutScheme.contains(":")) {
            return addressWithoutScheme.split(":")[0];
        }
        return addressWithoutScheme;
    }

    private String getScheme(String address) {
        return address.split(":")[0];
    }
}
