package com.cuupa.mailprocessor.services;

import com.cuupa.mailprocessor.configuration.application.ReminderProperties;
import com.cuupa.mailprocessor.delegates.scan.ReminderTriggerObject;
import com.cuupa.mailprocessor.services.reminder.telegram.ReminderBot;
import com.cuupa.mailprocessor.tos.semantic.Metadata;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class ReminderService {

    private final Log logger = LogFactory.getLog(ReminderService.class);

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public ReminderService() {
        ApiContextInitializer.init();
    }

    @NotNull
    private String getTextForReminder(String user, List<String> topics, String filename, String from, List<Metadata> metaData, String dueDate) {
        return "Das Schreiben \"" + getStringForTopics(topics,
                                                       user) + filename + "\" von \"" + from + "\" erfordert bis zum " + dueDate + " ihre Aufmerksamkeit.\n" + metaData
                .stream()
                .map(m -> m.toString() + "\n")
                .collect(Collectors.joining());
    }

    private String getStringForTopics(List<String> topics, String user) {
        return topics.stream()
                     .map(e -> Mappings.getMapping(e, user, Locale.GERMAN) + "_")
                     .collect(Collectors.joining());
    }

    private String getDueDate(List<Metadata> metaData) {
        String dueDate = "";
        for (Metadata data : metaData) {
            if ("dueDate".equals(data.getName())) {
                dueDate = data.getValue();
            }
        }
        return dueDate;
    }

    public synchronized void executeReminder(@NotNull ReminderTriggerObject to) throws TelegramApiException {
        ReminderProperties properties = new ReminderProperties(to.getUser());
        if (!properties.hasReminder()) {
            return;
        }

        TelegramBotsApi botsApi = new TelegramBotsApi();
        ReminderBot reminderBot = new ReminderBot(properties);
        try {
            botsApi.registerBot(reminderBot);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

        SendMessage message = new SendMessage() // Create a message object object
                                                .setChatId(properties.getChatId())
                                                .setText(getTextForReminder(to.getUser(),
                                                                            to.getTopics(),
                                                                            to.getFileName(),
                                                                            to.getFrom(),
                                                                            to.getMetaData(),
                                                                            to.getDueDate()));
        SendDocument document = new SendDocument().setChatId(properties.getChatId()).setDocument(to.getDocument());
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        rowInline.add(new InlineKeyboardButton().setText("Confirm").setCallbackData("update_msg_text"));
        // Set the keyboard to the markup
        rowsInline.add(rowInline);
        // Add it to the message
        markupInline.setKeyboard(rowsInline);
        document.setReplyMarkup(markupInline);
        reminderBot.execute(message);
        reminderBot.execute(document);
    }
}
