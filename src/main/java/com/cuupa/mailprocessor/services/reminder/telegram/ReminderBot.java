package com.cuupa.mailprocessor.services.reminder.telegram;

import com.cuupa.mailprocessor.configuration.application.ReminderProperties;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

public class ReminderBot extends TelegramLongPollingBot {

    private ReminderProperties reminderProperties;

    public ReminderBot(ReminderProperties reminderProperties) {
        this.reminderProperties = reminderProperties;
    }

    @Override
    public void onUpdateReceived(Update update) {

    }

    @Override
    public String getBotUsername() {
        return reminderProperties.getBotUsername();
    }

    @Override
    public String getBotToken() {
        return reminderProperties.getBotToken();
    }
}
