package com.cuupa.mailprocessor.services;

import com.cuupa.mailprocessor.delegates.email.ArchiveEmailDelegate;
import com.cuupa.mailprocessor.delegates.email.AttachmentDelegate;
import com.cuupa.mailprocessor.delegates.email.DownloadTypeDelegate;
import com.cuupa.mailprocessor.delegates.email.RoutingDelegate;
import com.cuupa.mailprocessor.process.MailInstanceHandler;
import com.cuupa.mailprocessor.services.input.email.ImapMailService;
import com.cuupa.mailprocessor.tos.mail.Mail;
import com.cuupa.mailprocessor.tos.mail.PDF;
import com.cuupa.mailprocessor.tos.mail.email.EMail;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;

public class ProcessService {

    private static final Log LOG = LogFactory.getLog(ProcessService.class);

    public static final String MAILPROCESSOR = "mailprocessor";

    @Autowired
    private RuntimeService runtimeService;

    public void startProcess(List<EMail> emails) {
        for (EMail eMail : emails) {
            try {

                if (!eMail.isArchived()) {
                    eMail = new DownloadTypeDelegate().execute(eMail);
                    if (eMail.isLoadAttachment()) {
                        eMail = new AttachmentDelegate().execute(eMail);
                    }

                    eMail = new RoutingDelegate().execute(eMail);
                    eMail = new ArchiveEmailDelegate(new ImapMailService()).execute(eMail);
                }
            } catch (Exception e) {
                LOG.error("Error Processing E-Mail " + eMail.getSubject(), e);
            }
        }
    }

    public void startProcess(Collection<PDF> files) {
        for (Mail pdf : files) {
            if (!pdf.isArchived()) {
                try {
                    MailInstanceHandler handler = new MailInstanceHandler(pdf);
                    runtimeService.startProcessInstanceByKey(MAILPROCESSOR, handler.getVariables());
                } catch (Exception e) {
                    e.printStackTrace();
                    LOG.error("Error Processing Mail " + pdf.getFileName(), e);
                }
            }
        }
    }
}
