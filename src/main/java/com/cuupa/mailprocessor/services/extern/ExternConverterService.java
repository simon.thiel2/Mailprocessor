package com.cuupa.mailprocessor.services.extern;


import com.cuupa.mailprocessor.configuration.application.ApplicationProperties;
import com.cuupa.mailprocessor.tos.document.Document;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ExternConverterService {

    private static final Gson gson = new Gson();

    private RestTemplate template = new RestTemplate();

    public Document convert(Document document) {
        String json = gson.toJson(document);

        ResponseEntity<String> response = template.postForEntity(ApplicationProperties.getConverterUrl(),
                                                                 json,
                                                                 String.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            return gson.fromJson(response.getBody(), Document.class);
        } else {
            throw new RuntimeException(response.getStatusCode().toString());
        }
    }

    @Autowired
    public void setRestTemplate(RestTemplate template) {
        this.template = template;
    }
}
