package com.cuupa.mailprocessor.services.extern;

import com.cuupa.mailprocessor.configuration.application.ApplicationProperties;
import com.cuupa.mailprocessor.tos.semantic.SemanticResult;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ExternSemanticService {

    private final RestTemplate template = new RestTemplate();

    private final Gson gson = new Gson();

    public List<SemanticResult> analize(final String text) {
        ResponseEntity<String> response = template.postForEntity(ApplicationProperties.getSemanticUrl(),
                                                                 text,
                                                                 String.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            Type listType = new TypeToken<ArrayList<SemanticResult>>() {
            }.getType();
            return gson.fromJson(response.getBody(), listType);
        } else {
            throw new RuntimeException();
        }
    }
}
