package com.cuupa.mailprocessor.delegates.email;

import com.cuupa.mailprocessor.configuration.application.DownloadTypeProperties;
import com.cuupa.mailprocessor.tos.mail.email.EMail;

public class DownloadTypeDelegate {

	public EMail execute(EMail eMail) {
		DownloadTypeProperties downloadTypeProperties = new DownloadTypeProperties(eMail.getUser());

		eMail.setLoadAttachment(downloadTypeProperties.isAttachmentToSave(eMail.getFrom()));
		eMail.setLoadMail(downloadTypeProperties.isMailToSave(eMail.getFrom()));

		return eMail;
	}
}
