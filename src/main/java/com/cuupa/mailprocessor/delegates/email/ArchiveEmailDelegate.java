package com.cuupa.mailprocessor.delegates.email;

import com.cuupa.mailprocessor.configuration.application.ArchiveProperties;
import com.cuupa.mailprocessor.services.archiver.WebDav;
import com.cuupa.mailprocessor.services.input.email.MailService;
import com.cuupa.mailprocessor.tos.mail.email.Attachment;
import com.cuupa.mailprocessor.tos.mail.email.EMail;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

public class ArchiveEmailDelegate {

    private static final Log LOG = LogFactory.getLog(ArchiveEmailDelegate.class);

    private final MailService mailService;

    public ArchiveEmailDelegate(MailService mailService) {
        this.mailService = mailService;
    }

    public EMail execute(EMail mail) throws Exception {
        mail = saveEMail(mail);
        if (mail.isArchived()) {
            mailService.markMailAsRead(mail);
        }
        return mail;
    }

    private EMail saveEMail(EMail mail) throws Exception {
        ArchiveProperties properties = new ArchiveProperties(mail.getUser());
        LOG.debug("Setting up webdav");
        try (WebDav webDav = new WebDav(properties.getUsername(), properties.getPassword())) {
            String lastPath = createCollections(mail.getPathToSave(), properties, webDav);
            uploadAttachments(mail, properties, webDav, mail.getAttachments());
            uploadFile(mail, properties, webDav);
        }
        mail.setArchived(true);
        return mail;
    }

    private void uploadAttachments(EMail mail, ArchiveProperties properties, WebDav webDav,
                                   List<Attachment> attachments) throws URISyntaxException, IOException {
        for (Attachment attachment : attachments) {
            URL url = getUrl(properties, mail.getPathToSave() + attachment.getFilename());

            if (!webDav.exists(url.toString())) {
                webDav.save(url.toString(), attachment.getContent());
            }
        }
    }

    private void uploadFile(EMail mail, ArchiveProperties properties, WebDav webDav) {
        File pdf = mail.getPdf();

        if (pdf != null) {
            // URL url = getUrl(mail, properties, pdf);

            // if(!webDav.exists(directory)) {
            // webDav.createDirectory(directory);
            // }

            // if (!webDav.exists(url.toString())) {
            // byte[] data = FileUtils.readFileToByteArray(pdf);
            // try {
            // webDav.put(url.toString(), data);
            // } catch (Exception e) {
            // LOG.error("Failed to archive " + pdf + " to " + url.getPath(), e);
            // }
            // }
        }
    }

    private URL getUrl(ArchiveProperties properties, String filename) throws MalformedURLException, URISyntaxException {
        return new URI(getScheme(properties.getAddress()), getHost(properties.getAddress()), filename, null).toURL();
    }

    private String getHost(String address) {
        return address.split("://")[1];
    }

    private String getScheme(String address) {
        LOG.debug("Address is: " + address);
        return address.split(":")[0];
    }

    private String createCollections(String path, ArchiveProperties properties, WebDav webDav)
            throws URISyntaxException, IOException {

        String[] paths = path.split("/");
        String pathTemp = "/";
        for (String part : paths) {
            if (part.length() > 0) {
                pathTemp += part + "/";
                URL urlPath = getUrl(properties, pathTemp);
                if (!webDav.exists(urlPath.toString())) {
                    webDav.createDirectory(urlPath.toString());
                }
            }
        }

        return pathTemp;
    }
}