package com.cuupa.mailprocessor.delegates.email;

import com.cuupa.mailprocessor.tos.mail.email.EMail;

public class RoutingDelegate {

    private static final String basepath = "/homes/%s/Drive/Meine Dokumente/";

	public EMail execute(EMail eMail) {
		if(eMail.getFrom().contains("rechnungsstelle@1und1.de")) {
			eMail.setPathToSave(_createPath(eMail.getPathToSave(), eMail.getUser()) + "Post/1&1 Internet AG/Rechnungen/");
		}
		
		else if(eMail.getFrom().contains("service@strato.de") && eMail.getLabel().equals("Rechnungen/Strato")) {
			eMail.setPathToSave(_createPath(eMail.getPathToSave(), eMail.getUser()) + "Post/Strato AG/Rechnungen/");
		}
		
		else if(eMail.getFrom().contains("@provinzial.com") && eMail.getLabel().equals("Gehaltsabrechnung")) {
			eMail.setPathToSave(_createPath(eMail.getPathToSave(), eMail.getUser()) + "Post/Gehaltsabrechnungen/Provinzial Rheinland Versicherung AG/");
		}
		return eMail;
	}
	
	
	private String _createPath(String path, String user) {
		String format = String.format(basepath, user);

		if (!path.contains(format)) {
			return format + path;
		} else {
			return path;
		}
	}
}
