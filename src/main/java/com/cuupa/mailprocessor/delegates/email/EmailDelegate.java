package com.cuupa.mailprocessor.delegates.email;

import com.cuupa.mailprocessor.services.extern.ExternConverterService;
import com.cuupa.mailprocessor.tos.document.Document;
import com.cuupa.mailprocessor.tos.document.DocumentBuilder;
import com.cuupa.mailprocessor.tos.mail.email.EMail;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class EmailDelegate {

	private ExternConverterService externConverterService;

	public void execute(EMail mail) throws Exception {

		String filename = mail.getEMLMessage();
		File file = new File(filename);
		Document document;
		try (InputStream inputStream = new FileInputStream(file)) {
			document = DocumentBuilder.create(inputStream, filename, (int) file.length()).build();
		}

		Document convertedDoc = externConverterService.convert(document);

		//File pdfFile = convertedDoc.writeToFile(ApplicationProperties.getWorkDir());

		//mail.setPdf(pdfFile);
	}

	@Autowired
	private void setExternConverterService(ExternConverterService service) {
		this.externConverterService = service;
	}
}
