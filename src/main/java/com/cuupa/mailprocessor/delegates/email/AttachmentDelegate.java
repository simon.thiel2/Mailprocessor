package com.cuupa.mailprocessor.delegates.email;

import com.cuupa.mailprocessor.services.extern.ExternConverterService;
import com.cuupa.mailprocessor.tos.document.Document;
import com.cuupa.mailprocessor.tos.document.DocumentBuilder;
import com.cuupa.mailprocessor.tos.mail.email.Attachment;
import com.cuupa.mailprocessor.tos.mail.email.EMail;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class AttachmentDelegate {

	private ExternConverterService externConverterService = new ExternConverterService();

	public EMail execute(EMail email) throws Exception {
		try (ByteArrayInputStream in = new ByteArrayInputStream(email.getContent())) {

			MimeMessage message = new MimeMessage(null, in);
			Multipart multipart = (Multipart) message.getContent();

			email = saveAttachments(email, multipart);
		}
		return email;
	}

	private EMail saveAttachments(EMail email, Multipart multipart)
			throws MessagingException, IOException {
		for (int i = 0; i < multipart.getCount(); i++) {
			BodyPart bodyPart = multipart.getBodyPart(i);

			String filename = bodyPart.getFileName();
			if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) && StringUtils.isBlank(filename)) {
				continue;
			}

			Document document = DocumentBuilder.create(bodyPart.getInputStream(), filename, bodyPart.getSize()).build();

			Document convertedDoc = externConverterService.convert(document);

			Attachment attachment = new Attachment(convertedDoc.getFileName(), convertedDoc.getContent());
			attachment.setEncrypted(convertedDoc.isEncrypted());
			email.addAttachment(attachment);
		}
		return email;
	}

	@Autowired
	public void setExternConverterService(ExternConverterService service) {
		this.externConverterService = service;
	}
}
