package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.configuration.application.ArchiveProperties;
import com.cuupa.mailprocessor.configuration.application.ScanProperties;
import com.cuupa.mailprocessor.process.MailInstanceHandler;
import com.cuupa.mailprocessor.services.archiver.FileProtocol;
import com.cuupa.mailprocessor.services.archiver.FileProtocolFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class ArchiveScanDelegate implements JavaDelegate {

	private static final Log LOG = LogFactory.getLog(ArchiveScanDelegate.class);

    @Override
    public void execute(DelegateExecution execution) {
        MailInstanceHandler handler = new MailInstanceHandler(execution);

		handler.setPathToSave(handler.getPathToSave().replace("%sender%", handler.getFrom()));

        ArchiveProperties properties = new ArchiveProperties(handler.getUser());

		try (FileProtocol fileProtocol = FileProtocolFactory.get(properties.getAddress(),
																 properties.getUsername(),
																 properties.getPassword())) {

            LOG.debug("Setting up ArchiveService");

            String lastPath = createCollections(handler.getPathToSave(), properties, fileProtocol);

            ScanProperties props = new ScanProperties(handler.getUser());

			String filename = null;
			for (String prefix : props.getScannerPrefix()) {
                if (handler.getFileName().startsWith(prefix)) {
					filename = "[" + handler.getTopics()
											.stream()
											.collect(Collectors.joining("_")) + "]_" + handler.getFileName();
				}
			}

            if (filename == null) {
                filename = handler.getFileName();
			}
			filename = filename.replace(" ", "_");

			URL urlFile = getUrl(properties, lastPath + filename);
            if (!fileProtocol.exists(urlFile.toString())) {
                fileProtocol.save(urlFile.toString(), handler.getContent());
				System.out.println("Archiviert nach: " + urlFile.toString());
			}
			//deleteWorkCopy(handler, properties);
		} catch (Exception e) {
            LOG.error("Failed to archive " + handler.getFileName(), e);
		}
	}

	private URL getWorkUrl(ScanProperties scanProperties, String folder, String name)
			throws MalformedURLException, URISyntaxException {
		return new URI(getScheme(scanProperties.getAddress()),
					   null,
					   getHost(scanProperties.getAddress()),
					   getPort(scanProperties.getAddress()),
					   "/" + folder + "/" + name,
					   null,
					   null).toURL();
	}

    private String createCollections(String path, ArchiveProperties properties, FileProtocol fileProtocol)
			throws URISyntaxException, IOException {

		String[] paths = path.split("/");
		String pathTemp = "/";
		for (String part : paths) {
			if (part.length() > 0) {
				pathTemp += part + "/";
				URL urlPath = getUrl(properties, pathTemp);
				String urlString = new String(urlPath.toString().getBytes(StandardCharsets.UTF_8),
											  StandardCharsets.UTF_8);
				if (!fileProtocol.exists(urlString)) {
					LOG.error("Creating collection " + urlString);
					fileProtocol.createDirectory(urlString);
				}
			}
		}
		return pathTemp;
	}

    private void deleteWorkCopy(MailInstanceHandler handler, ArchiveProperties archiveProperties) {
        ScanProperties scanProperties = new ScanProperties(handler.getUser());

		try (FileProtocol fileProtocol = FileProtocolFactory.get(scanProperties.getAddress(),
																 scanProperties.getUsername(),
																 scanProperties.getPassword())) {
            final String url = getWorkUrl(scanProperties, scanProperties.getFolder(), handler.getFileName()).toString();
			fileProtocol.delete(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private URL getUrl(ArchiveProperties properties, String path) throws MalformedURLException, URISyntaxException {
		return new URI(getScheme(properties.getAddress()),
					   null,
					   getHost(properties.getAddress()),
					   getPort(properties.getAddress()),
					   path,
					   null,
					   null).toURL();
	}


	private int getPort(String address) {
		if (address.contains(":")) {
			String[] addressArray = address.split(":");
			String port = addressArray[addressArray.length - 1];
			return Integer.parseInt(port);
		}
		return -1;
	}

	private String getHost(String address) {
		String addressWithoutScheme = address.split("://")[1];
		if (addressWithoutScheme.contains(":")) {
			return addressWithoutScheme.split(":")[0];
		}
		return addressWithoutScheme;
	}

	private String getScheme(String address) {
		return address.split(":")[0];
	}
}
