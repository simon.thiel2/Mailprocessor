package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.tos.semantic.Metadata;
import org.telegram.telegrambots.meta.api.objects.InputFile;

import java.util.List;

public class ReminderTriggerObject {

    private String user;


    private List<String> topics;

    private String from;

    private String fileName;

    private List<Metadata> metaData;

    private String dueDate;

    private InputFile document;

    public ReminderTriggerObject withUser(String user) {
        this.user = user;
        return this;
    }

    public ReminderTriggerObject withTopics(List<String> topics) {
        this.topics = topics;
        return this;
    }

    public ReminderTriggerObject withFrom(String from) {
        this.from = from;
        return this;
    }

    public ReminderTriggerObject withFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public ReminderTriggerObject withMetaData(List<Metadata> metaData) {
        this.metaData = metaData;
        return this;
    }

    public ReminderTriggerObject withDueDate(String dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public ReminderTriggerObject withDocument(InputFile document) {
        this.document = document;
        return this;
    }

    public String getUser() {
        return user;
    }

    public List<String> getTopics() {
        return topics;
    }

    public String getFrom() {
        return from;
    }

    public String getFileName() {
        return fileName;
    }

    public List<Metadata> getMetaData() {
        return metaData;
    }

    public String getDueDate() {
        return dueDate;
    }

    public InputFile getDocument() {
        return document;
    }
}
