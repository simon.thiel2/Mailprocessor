package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.process.MailInstanceHandler;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Simon Thiel (https://github.com/cuupa)
 */
public class DetectReminderDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        MailInstanceHandler handler = new MailInstanceHandler(delegateExecution);

        if (handler.hasReminder()) {
            handler.setReminderDate(LocalDate.now().format(DateTimeFormatter.ISO_DATE));
            delegateExecution.setVariables(handler.getVariables());
            return;
        }

        if (handler.getReminderDates().isEmpty()) {
            handler.setReminderDates(handler.getMetaData()
                                            .stream()
                                            .filter(e -> "dueDate".equals(e.getName()))
                                            .map(e -> LocalDate.parse(e.getValue(),
                                                                      DateTimeFormatter.ofPattern("dd.MM.yyyy")))
                                            .map(e -> e.format(DateTimeFormatter.ISO_DATE))
                                            .collect(Collectors.toList()));
        }

        Optional<String> minDate = handler.getReminderDates()
                                          .stream()
                                          .distinct()
                                          .min(Comparator.comparing(String::valueOf));
        if (minDate.isPresent()) {
            handler.setHasReminder(true);
            handler.setReminderDate(minDate.get());
            handler.getReminderDates().remove(minDate.get());
        } else {
            handler.setHasReminder(false);
        }
        delegateExecution.setVariables(handler.getVariables());
    }
}
