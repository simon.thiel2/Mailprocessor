package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.process.MailInstanceHandler;
import com.cuupa.mailprocessor.tos.semantic.Metadata;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Simon Thiel (https://github.com/cuupa)
 */
public class DmnResultMapperDelegate implements JavaDelegate {

    private final Pattern regexPlaceholder = Pattern.compile("\\%[a-zA-Z]*\\%");

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        MailInstanceHandler handler = new MailInstanceHandler(delegateExecution);
        String pathToSave = getPathToSave(delegateExecution);
        pathToSave = replacePlaceholder(pathToSave, handler);
        handler.setPathToSave(pathToSave);
        handler.setHasReminder(getRemider(delegateExecution));
        delegateExecution.setVariables(handler.getVariables());
    }

    private String replacePlaceholder(String pathToSave, final MailInstanceHandler handler) {

        if (StringUtils.isNotEmpty(handler.getFrom())) {
            pathToSave = pathToSave.replace("%sender%", handler.getFrom());
        }

        final Matcher matcher = regexPlaceholder.matcher(pathToSave);
        while (matcher.find()) {
            final String varName = matcher.group();
            final List<Metadata> collect = handler.getMetaData()
                                                  .stream()
                                                  .filter(e -> e.getName().equals(varName.replace("%", "")))
                                                  .collect(Collectors.toList());
            pathToSave = pathToSave.replace(varName,
                                            collect.stream().map(Metadata::getValue).collect(Collectors.joining("_")));
        }
        return pathToSave;
    }

    private Boolean getRemider(DelegateExecution delegateExecution) {
        Map<String, Object> dmn_result = (Map<String, Object>) delegateExecution.getVariable("dmn_result");
        return (Boolean) dmn_result.get("hasReminder");
    }

    private String getPathToSave(DelegateExecution delegateExecution) {
        Map<String, Object> dmn_result = (Map<String, Object>) delegateExecution.getVariable("dmn_result");
        return (String) dmn_result.get("path_to_save");
    }
}
