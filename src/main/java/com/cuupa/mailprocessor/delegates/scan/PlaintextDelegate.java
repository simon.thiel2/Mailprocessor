package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.process.MailInstanceHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

public class PlaintextDelegate implements JavaDelegate {

	private static final Log LOG = LogFactory.getLog(PlaintextDelegate.class);

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		MailInstanceHandler handler = new MailInstanceHandler(execution);

		try (PDDocument document = PDDocument.load(new ByteArrayInputStream(handler.getContent()))) {

			List<String> pages = new ArrayList<>(document.getNumberOfPages());
			for (int page = 1; page <= document.getNumberOfPages(); page++) {
				PDFTextStripper stripper = new PDFTextStripper();
				stripper.setStartPage(page);
				stripper.setEndPage(page);
				pages.add(stripper.getText(document));
			}

			handler.setPlainText(pages);
			execution.setVariables(handler.getVariables());
		} finally {
			LOG.info("Found " + handler.getPlainText().size() + " pages in document " + handler.getFileName());
		}
	}
}
