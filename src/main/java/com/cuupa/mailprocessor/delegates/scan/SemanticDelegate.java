package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.process.MailInstanceHandler;
import com.cuupa.mailprocessor.services.extern.ExternSemanticService;
import com.cuupa.mailprocessor.tos.semantic.SemanticResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.List;
import java.util.stream.Collectors;

public class SemanticDelegate implements JavaDelegate {

    private static final Log LOG = LogFactory.getLog(SemanticDelegate.class);

    private final ExternSemanticService externSemanticService;

    public SemanticDelegate(ExternSemanticService externSemanticService) {
        this.externSemanticService = externSemanticService;
    }

    @Override
    public void execute(DelegateExecution execution) {
        MailInstanceHandler handler = new MailInstanceHandler(execution);
        List<String> plainText = handler.getPlainText();

        if (plainText.isEmpty()) {
            return;
        }

        List<SemanticResult> semanticResult = externSemanticService.analize(plainText.stream()
                                                                                     .map(Object::toString)
                                                                                     .collect(Collectors.joining()));

        semanticResult.forEach(result -> {
            handler.addTopic(result.getTopicName());
            handler.setFrom(result.getSender());
            if (result.getMetaData() != null) {
                handler.addMetaData(result.getMetaData()
                                          .stream()
                                          .filter(e -> !"sender".equals(e.getName()))
                                          .collect(Collectors.toList()));
            }
        });


        LOG.debug("Document from sender " + "'" + handler.getFrom() + "' classified as " + handler.getTopics());
        execution.setVariables(handler.getVariables());
    }

    private String getSender(String plaintext) {
        if (plaintext.contains("Stuttgarter Lebensversicherung")) {
            return "Stuttgarter Lebensversicherung AG";
        }

        if (plaintext.contains("TÜVRheinland")) {
            return "TÜV Rheinland";
        }

        if (plaintext.contains("Zalando SE")) {
            return "Zalando SE";
        }

        if (plaintext.contains("ARD") && plaintext.contains("Beitragsservice") && plaintext.contains("ZDF")) {
            return "ARD ZDF Deutschlandradio Beitragsservice";
        }

        if (plaintext.contains("Provinzial Rheinland Versicherung AG") && !plaintext.contains(
                "Stadtsparkasse Düsseldorf") && plaintext.contains("Die Versicherung der Sparkassen") || plaintext.contains(
                "Provinzial Rheinland") || ((plaintext.contains("Provinzial") || plaintext.contains("PROVINZIAL")) && (plaintext
                .contains("Die Versicherung der Sparkassen") || plaintext.contains("Die Versicherung der s Sparkassen")))) {
            return "Provinzial Rheinland Versicherung AG";
        }

        if (plaintext.contains("Provinzial Rheinland Lebensversicherung AG") && plaintext.contains(
                "Die Versicherung der Sparkassen")) {
            return "Provinzial Rheinland Lebensversicherung AG";
        }

        if ((plaintext.contains("ARAG Versicherungen") && plaintext.contains("ARAG Platz 1")) || plaintext.contains(
                "ARAG SE")) {
            return "ARAG Versicherungen";
        }

        if (plaintext.contains("ebase") && plaintext.contains("European Bank for Financial Services GmbH")) {
            return "ebase";
        }

        if (plaintext.contains("tecis") && plaintext.contains("tecis Finanzdienstleistungen AG")) {
            return "tecis";
        }

        if (plaintext.contains("LBS Westdeutsche Landesbausparkasse") || plaintext.contains(
                "Bausparkasse der Sparkassen") && plaintext.contains("LBS")) {
            return "LBS Westdeutsche Landesbausparkasse";
        }

        if (plaintext.contains("WWK Lebensversicherung") || plaintext.contains("Eine starke Gemeinschaft")) {
            return "WWK Lebensversicherung AG";
        }

        if (plaintext.contains("Deutsche Bank Privat und Geschäftskunden AG")) {
            return "Deutsche Bank Privat- und Geschäftskunden AG";
        }

        if (plaintext.contains("SWK MOBIL GmbH") && plaintext.contains("SWK STADTWERKE")) {
            return "SWK MOBIL GmbH";
        }

        if (plaintext.contains("Stadtsparkasse Düsseldorf") || plaintext.contains("Stadt Sparkasse Düsseldorf")) {
            return "Stadtsparkasse Düsseldorf";
        }

        if (plaintext.contains("Sparkasse Krefeld")) {
            return "Sparkasse Krefeld";
        }

        if (plaintext.contains("Techniker Krankenkasse")) {
            return "Techniker Krankenkasse";
        }

        if (plaintext.contains("1&1 Internet AG") || plaintext.contains("1&1 Telecom GmbH")) {
            return "1&1 Internet AG";
        }

        if (plaintext.contains("Stadtwerke Düsseldorf AG")) {
            return "Stadtwerke Düsseldorf AG";
        }

        if (plaintext.contains("RAY Property Manag. GmbH") || plaintext.contains("RAY Property Management GmbH")) {
            return "RAY Property Management GmbH";
        }

        if ((plaintext.contains("Arbeitsunfähigkeitsbescheinigung") || plaintext.contains("arbeitsunfähig seit")) && plaintext
                .contains("ICD 10")) {
            return "Arbeitsunfähigkeitsbescheinigung";
        }

        if (plaintext.contains("DKMS")) {
            return "DKMS";
        }

        if (plaintext.contains("Amazon")) {
            return "Amazon";
        }

        if (plaintext.contains("United Kiosk AG")) {
            return "United Kiosk AG";
        }

        if (plaintext.contains("Deutsche Rentenversicherung Bund")) {
            return "Deutsche Rentenversicherung Bund";
        }

        if (plaintext.contains("camunda") && plaintext.contains("camunda Services GmbH") && plaintext.contains(
                "www.camunda.com")) {
            return "camunda";
        }

        if (plaintext.contains("Höffner Möbelgesellschaft GmbH")) {
            return "Höffner Möbelgesellschaft GmbH";
        }

        return null;
    }
}
