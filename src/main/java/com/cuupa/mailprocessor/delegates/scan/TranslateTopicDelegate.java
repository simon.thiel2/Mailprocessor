package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.process.MailInstanceHandler;
import com.cuupa.mailprocessor.services.Mappings;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.Locale;
import java.util.stream.Collectors;

public class TranslateTopicDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        MailInstanceHandler handler = new MailInstanceHandler(execution);
        handler.setTopics(handler.getTopics()
                                 .stream()
                                 .map(e -> Mappings.getMapping(e, handler.getUser(), Locale.getDefault()))
                                 .collect(Collectors.toList()));
        execution.setVariables(handler.getVariables());
    }

}
