package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.process.MailInstanceHandler;
import com.cuupa.mailprocessor.services.ReminderService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.ByteArrayInputStream;
import java.util.Comparator;
import java.util.List;

public class TriggerReminderDelegate implements JavaDelegate {

    private ReminderService reminderService;

    public TriggerReminderDelegate(ReminderService reminderService) {
        this.reminderService = reminderService;
        ApiContextInitializer.init();
    }

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        MailInstanceHandler handler = new MailInstanceHandler(delegateExecution);
        String dueDate = getReminderDate(handler.getReminderDates());
        ReminderTriggerObject to = new ReminderTriggerObject().withUser(handler.getUser())
                                                              .withTopics(handler.getTopics())
                                                              .withFrom(handler.getFrom())
                                                              .withMetaData(handler.getMetaData())
                                                              .withFileName(handler.getFileName())
                                                              .withDueDate(dueDate)
                                                              .withDocument(createInputFile(handler.getContent(),
                                                                                            handler.getFileName()));
        try {
            reminderService.executeReminder(to);
            handler.getReminderDates().remove(dueDate);
            handler.setHasReminder(false);
            handler.setReminderDate(null);
            delegateExecution.setVariables(handler.getVariables());
        } catch (TelegramApiException e) {
            throw e;
        }
    }

    private InputFile createInputFile(byte[] content, String filename) {
        return new InputFile(new ByteArrayInputStream(content), filename);
    }

    private String getReminderDate(List<String> reminderDates) {
        return reminderDates.stream().min(Comparator.comparing(String::valueOf)).orElse(null);
    }
}