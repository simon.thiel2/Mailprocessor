package com.cuupa.mailprocessor.process;

import com.cuupa.mailprocessor.tos.mail.Mail;
import com.cuupa.mailprocessor.tos.semantic.Metadata;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Simon Thiel (https://github.com/cuupa)
 */
public class MailInstanceHandler {

    private final Map<String, Object> variables = new HashMap<>();

    {
        for (MailProcessProperty property : MailProcessProperty.values()) {
            variables.put(property.getName(), null);
        }
    }

    public MailInstanceHandler(DelegateExecution execution) {
        variables.clear();
        variables.putAll(execution.getVariables());
    }

    public MailInstanceHandler(Mail mail) {
        variables.put(MailProcessProperty.CONTENT.getName(), mail.getContent());
        variables.put(MailProcessProperty.FILENAME.getName(), mail.getFileName());
        variables.put((MailProcessProperty.FROM.getName()), mail.getFrom());
        variables.put(MailProcessProperty.META_DATA.getName(), mail.getMetaData());
        variables.put(MailProcessProperty.PATH_TO_SAVE.getName(), mail.getPathToSave());
        variables.put(MailProcessProperty.PLAIN_TEXT.getName(), mail.getPlainText());
        variables.put(MailProcessProperty.RECEIVED_DATE.getName(), mail.getReceivedDate());
        variables.put(MailProcessProperty.TOPICS.getName(), mail.getTopics());
        variables.put(MailProcessProperty.USER.getName(), mail.getUser());
    }

    public byte[] getContent() {
        return getByteArray(variables.get(MailProcessProperty.CONTENT.getName()));
    }

    public List<String> getPlainText() {
        return getAsList(variables.get(MailProcessProperty.PLAIN_TEXT.getName()));
    }

    public void setPlainText(List<String> pages) {
        variables.put(MailProcessProperty.PLAIN_TEXT.getName(), pages);
    }

    private List<String> getAsList(Object o) {
        return (List) o;
    }

    private byte[] getByteArray(Object o) {
        return (byte[]) o;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void addTopic(String topicName) {
        getAsList(variables.get(MailProcessProperty.TOPICS.getName())).add(topicName);
    }

    public void addMetaData(List<Metadata> metadata) {
        variables.put(MailProcessProperty.META_DATA.getName(), metadata);
    }

    public String getFrom() {
        return (String) variables.get(MailProcessProperty.FROM.getName());
    }

    public void setFrom(String sender) {
        variables.put(MailProcessProperty.FROM.getName(), sender);
    }

    public List<String> getTopics() {
        return (List<String>) variables.get(MailProcessProperty.TOPICS.getName());
    }

    public void setTopics(List<String> topics) {
        variables.put(MailProcessProperty.TOPICS.getName(), topics);
    }

    public String getUser() {
        return (String) variables.get(MailProcessProperty.USER.getName());
    }

    public List<Metadata> getMetaData() {
        return (List<Metadata>) variables.get(MailProcessProperty.META_DATA.getName());
    }

    public String getFileName() {
        return (String) variables.get(MailProcessProperty.FILENAME.getName());
    }

    public String getPathToSave() {
        return (String) variables.get(MailProcessProperty.PATH_TO_SAVE.getName());
    }

    public void setPathToSave(String pathToSave) {
        variables.put(MailProcessProperty.PATH_TO_SAVE.getName(), pathToSave);
    }

    public void setHasReminder(Boolean reminder) {
        variables.put("hasReminder", reminder);
    }

    public boolean hasReminder() {
        Object hasReminder = variables.getOrDefault("hasReminder", false);
        if (hasReminder == null) {
            return false;
        }
        return (boolean) hasReminder;
    }

    public void setReminderDate(String localDate) {
        variables.put("reminderDate", localDate);
    }

    public List<String> getReminderDates() {
        return (List<String>) variables.getOrDefault("reminderDates", new ArrayList<String>());
    }

    public void setReminderDates(List<String> reminderDates) {
        variables.put("reminderDates", reminderDates);
    }
}
