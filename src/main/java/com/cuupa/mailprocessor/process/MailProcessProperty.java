package com.cuupa.mailprocessor.process;

/**
 * @author Simon Thiel (https://github.com/cuupa)
 */
public enum MailProcessProperty {

    TOPICS("topics"),
    META_DATA("meta_data"),
    FROM("from"),
    RECEIVER("receiver"),
    RECEIVED_DATE("received_date"),
    FILENAME("filename"),
    PATH_TO_SAVE("path_to_save"),
    CONTENT("content"),
    PLAIN_TEXT("plain_text"),
    USER("user");

    private String name;

    MailProcessProperty(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
