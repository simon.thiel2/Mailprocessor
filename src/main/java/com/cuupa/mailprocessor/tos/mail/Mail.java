package com.cuupa.mailprocessor.tos.mail;

import com.cuupa.mailprocessor.tos.semantic.Metadata;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Mail {

    private final List<String> topics = new ArrayList<>();
    private String pathToSave = "";
    private boolean archived;
    private String from;
    private Date receivedDate;
    private List<String> plainText;
    private String fileName;
    private String user;

    private byte[] content;
    private final List<Metadata> metaData = new ArrayList<>();

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setPathToSave(String pathToSave) {
        this.pathToSave = pathToSave;
    }

    public String getPathToSave() {
        return pathToSave;
    }

    public void addTopic(String topic) {
        topics.add(topic);
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setPlainText(List<String> plainText) {
        this.plainText = plainText;
    }

    public List<String> getPlainText() {
        return plainText;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public void addMetaData(List<Metadata> metaData) {
        this.metaData.addAll(metaData);
    }

    public List<Metadata> getMetaData() {
        return metaData;
    }

    public boolean hasMetaDates() {
        return metaData != null && metaData.size() > 0;
    }
}
