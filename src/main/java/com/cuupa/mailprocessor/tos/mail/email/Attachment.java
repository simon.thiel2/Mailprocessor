package com.cuupa.mailprocessor.tos.mail.email;

import java.io.Serializable;

public class Attachment implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String filename;
	
	private boolean encrypted;

	private final byte[] content;

	public Attachment(String filename, byte[] content) {
		this.filename = filename;
		this.content = content;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}

	public String getFilename() {
		return filename;
	}

	public boolean isEncrypted() {
		return encrypted;
	}

	public byte[] getContent() {
		return content;
	}
}
