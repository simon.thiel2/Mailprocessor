package com.cuupa.mailprocessor.tos.mail;

import java.util.Date;

public class MailDocument {

	private byte[] content;
	private Date receivedDate;
	private String fileName;

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceiveDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}
}
