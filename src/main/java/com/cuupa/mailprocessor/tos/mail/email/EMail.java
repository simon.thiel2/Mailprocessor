package com.cuupa.mailprocessor.tos.mail.email;

import com.cuupa.mailprocessor.tos.mail.Mail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EMail extends Mail {

	private boolean loadMail;
	private boolean loadAttachment;
	private String subject;
	private String emlMessage;

	private final List<Attachment> attachments = new ArrayList<>();
	private String label;
	
	public void setLoadMail(boolean loadMail) {
		this.loadMail = loadMail;
	}

	public void setLoadAttachment(boolean loadAttachment) {
		this.loadAttachment = loadAttachment;
	}

	public boolean isLoadAttachment() {
		return loadAttachment;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setEMLMessage(String emlMessage) {
		this.emlMessage = emlMessage;
	}

	public String getEMLMessage() {
		return emlMessage;
	}

	public void addAttachment(Attachment attachment) {
		attachments.add(attachment);
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}


	public File getPdf() {
		return null;
	}

	public void setPdf(File pdfFile) {
	}
}
