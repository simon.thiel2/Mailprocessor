package com.cuupa.mailprocessor.tos.mail.email;

public enum EMailProperties {
	
	encrypted, 
	subject, 
	_loadMail, 
	_loadAttachment, 
	message, 
	attachments, 
	label, 
	pdf, 
	user
}
