package com.cuupa.mailprocessor.tos.document;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

public class Document implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private byte[] content;
	
	private String filename;

	private boolean encrypted;

	public void setContent(byte[] content) {
		this.content = content;
	}

	public void setFileName(String filename) {
		this.filename = filename;
	}

	public byte[] getContent() {
		return content;
	}

	public String getFileName() {
		return filename;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}

	public boolean isEncrypted() {
		return encrypted;
	}

	public File writeToFile(String workFolder) throws IOException {
		File file = new File(workFolder + File.separator + filename);
		try (OutputStream fos = new BufferedOutputStream(new FileOutputStream(file))) {
			fos.write(content);
			return file;
		}
	}
}
