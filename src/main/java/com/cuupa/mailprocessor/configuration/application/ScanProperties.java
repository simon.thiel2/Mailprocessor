package com.cuupa.mailprocessor.configuration.application;

import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class ScanProperties {

	private static final Log LOG = LogFactory.getLog(ScanProperties.class);

	private final Properties properties = new Properties();

	public ScanProperties(String name) {
		File file = new File(ApplicationProperties.getConfigurationDir() + ApplicationProperties.getFileSeperator()
									 + name + ApplicationProperties.getFileSeperator() + "scan.properties");

		if (file.exists()) {
			try (InputStream in = new FileInputStream(file)) {
				properties.load(in);
			} catch (IOException e) {
				LOG.error("Unable to load mailproperties file", e);
			}
		}
	}

	public String getFolder() {
		return properties.getProperty("folder");
	}

	public String getAddress() {
		return properties.getProperty("address");
	}

	public List<String> getScannerPrefix() {
		return getScannerPrefixes(properties.getProperty("scanner_prefix"));
	}

	private List<String> getScannerPrefixes(String propertyValue) {
		if (propertyValue.contains(",")) {
			return Arrays.stream(propertyValue.split(",")).map(String::trim).collect(Collectors.toList());
		} else {
			return Lists.newArrayList(propertyValue);
		}
	}

	public String getPassword() {
		return properties.getProperty("password");
	}

	public String getUsername() {
		return properties.getProperty("username");
	}
}
