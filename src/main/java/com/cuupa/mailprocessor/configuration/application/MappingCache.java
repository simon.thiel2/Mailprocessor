package com.cuupa.mailprocessor.configuration.application;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

public class MappingCache {

	private final Map<String, Map<Locale, Properties>> cache = new HashMap<>();
	
	public boolean contains(String user) {
		return cache.containsKey(user);
	}

	public void put(String user, Map<Locale, Properties> loadMappings) {
		cache.put(user, loadMappings);
	}

	public Properties get(String user, Locale locale) {
		return cache.get(user).getOrDefault(locale, new Properties());
	}
}
