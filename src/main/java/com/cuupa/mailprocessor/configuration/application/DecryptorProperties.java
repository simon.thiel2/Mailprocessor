package com.cuupa.mailprocessor.configuration.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DecryptorProperties {
	
	private static final Log LOG = LogFactory.getLog(DecryptorProperties.class);

	private final Properties properties = new Properties();

	public DecryptorProperties(String user) {
		File file = new File(ApplicationProperties.getConfigurationDir() + ApplicationProperties.getFileSeperator()
				+ user + ApplicationProperties.getFileSeperator() + "decryptor.properties");

		if (file.exists()) {
			try (InputStream in = new FileInputStream(file)) {
				properties.load(in);
			} catch (IOException e) {
				LOG.error("couldn't read config file", e);
			}
		}

	}

	public String getPassword(String from) {
		return properties.getProperty(from);
	}
}
