package com.cuupa.mailprocessor.configuration.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

public class DownloadTypeProperties {

	private static final Log LOG = LogFactory.getLog(DownloadTypeProperties.class);

	private final Properties properties = new Properties();

	public DownloadTypeProperties(String user) {
		init(user);
	}

	private void init(String user) {
		File file = new File(ApplicationProperties.getConfigurationDir() + user
				+ ApplicationProperties.getFileSeperator() + "download.properties");

		if (file.exists()) {
			try (InputStream in = new FileInputStream(file)) {
				properties.load(in);
			} catch (IOException e) {
				LOG.error("couldn't read config file", e);
			}
		}
	}

	public boolean isAttachmentToSave(String sender) {
		sender = _resolveSender(sender);
		String property = properties.getProperty(sender, "false");
		return _find(property, "attachment");
	}

	public boolean isMailToSave(String sender) {
		sender = _resolveSender(sender);
		String property = properties.getProperty(sender, "false");
		return _find(property, "mail");
	}

	private String _resolveSender(String sender) {
		if (!properties.containsKey(sender) && sender.contains("@")) {
			sender = sender.split("@")[1];
		}

		if (!properties.containsKey(sender) && sender.contains(".")) {
			sender = sender.split("\\.")[0];
		}
		return sender;
	}

	private Boolean _find(String property, String type) {
		String[] split = property.split(",");
		return Arrays.stream(split)
					 .map(string -> string.trim().split(":"))
					 .filter(prop -> prop[0].equals(type))
					 .findFirst()
					 .filter(prop -> Boolean.parseBoolean(prop[1]))
					 .isPresent();
	}
}
