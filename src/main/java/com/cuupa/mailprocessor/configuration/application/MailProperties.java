package com.cuupa.mailprocessor.configuration.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;


public class MailProperties {

	private static final Log LOG = LogFactory.getLog(MailProperties.class);

	private final Properties properties = new Properties();

	public MailProperties(String user) {
		File file = new File(ApplicationProperties.getConfigurationDir() + ApplicationProperties.getFileSeperator()
									 + user + ApplicationProperties.getFileSeperator() + "mail.properties");

		if (file.exists()) {
			try (InputStream in = new FileInputStream(file)) {
				properties.load(in);
			} catch (IOException e) {
				LOG.error("Unable to load mailproperties file", e);
			}
		}
	}

	public String getServername() {
		return properties.getProperty("servername");
	}

	public String getPassword() {
		return properties.getProperty("password");
	}

	public String getUsername() {
		return properties.getProperty("username");
	}

	public List<String> getLabels() {
		String values = properties.getProperty("labels");
		String[] split = values.split(",");
		return Arrays.stream(split).map(String::trim).collect(Collectors.toList());
	}
}
