package com.cuupa.mailprocessor.configuration.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class ApplicationProperties {

    private static final Log LOG = LogFactory.getLog(ApplicationProperties.class);

    private static final Properties properties = new Properties();

    private static final Properties serverProperties = new Properties();

    static {
        _initServerEnvironment();

        File file = new File(serverProperties.getProperty("configDir") + "application.properties");

        try (InputStream in = new FileInputStream(file)) {
            properties.load(in);
        } catch (IOException e) {
            LOG.error("couldn't read config file", e);
        }
    }

    private static void _initServerEnvironment() {
        if (System.getProperty("jboss.server.config.dir") != null) {
            _setJBoss();
        } else if (System.getProperty("catalina.base") != null) {
            _setTomcat();
        } else {
            _setJarOrUnitTest();
        }

        LOG.debug("detected environment: " + serverProperties.getProperty("serverEnvironment"));
        LOG.debug("set config dir to : " + serverProperties.getProperty("configDir"));
    }

    private static void _setJarOrUnitTest() {
        serverProperties.setProperty("serverEnvironment", "local or unittest");
        serverProperties.setProperty("configDir",
                                     "src" + getFileSeperator() + "test" + getFileSeperator() + "resources" + getFileSeperator() + "conf" + getFileSeperator());
    }

    private static void _setTomcat() {
        serverProperties.setProperty("serverEnvironment", "tomcat");
        serverProperties.setProperty("configDir",
                                     System.getProperty("catalina.base") + getFileSeperator() + "conf" + getFileSeperator() + "mailprocessor/mailprocessor" + getFileSeperator());
    }

    private static void _setJBoss() {
        serverProperties.setProperty("serverEnvironment", "jboss");
        serverProperties.setProperty("configDir",
                                     System.getProperty("jboss.server.config.dir") + getFileSeperator() + "mailprocessor" + getFileSeperator());
    }

    public static List<String> getUsers() {
        String values = properties.getProperty("users");
        String[] split = values.split(",");
        return Arrays.stream(split).map(String::trim).collect(Collectors.toList());
    }

    public static String getFileSeperator() {
        return System.getProperty("file.separator");
    }

    public static String getConfigurationDir() {
        return serverProperties.getProperty("configDir");
    }

    public static List<String> getAllowedFileTypes() {
        String values = properties.getProperty("allowedFileTypes");
        String[] split = values.split(",");
        return Arrays.stream(split).map(String::trim).collect(Collectors.toList());
    }

    public static String getConverterUrl() {
        return properties.getProperty("converterurl");
    }

    public static String getSemanticUrl() {
        return properties.getProperty("semanticurl");
    }

    static String getMappingDir() {
        return properties.getProperty("mappingDir", "");
    }
}
