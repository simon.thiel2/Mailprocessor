package com.cuupa.mailprocessor.configuration.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ArchiveProperties {

	private static final Log LOG = LogFactory.getLog(ArchiveProperties.class);

	private final Properties properties = new Properties();

	public ArchiveProperties(String user) {
		init(user);
	}

	private void init(String user) {
		File file = new File(ApplicationProperties.getConfigurationDir() + user
									 + ApplicationProperties.getFileSeperator() + "archive.properties");

		if (file.exists()) {
			try (InputStream in = new FileInputStream(file)) {
				properties.load(in);
			} catch (IOException e) {
				LOG.error("couldn't read config file", e);
			}
		}
	}

	public String getUsername() {
		return properties.getProperty("username");
	}

	public String getProperties() {
		return properties.getProperty("password");
	}

	public String getAddress() {
		return properties.getProperty("address");
	}

	public String getPassword() {
		return properties.getProperty("password");
	}
}
