package com.cuupa.mailprocessor.configuration.application;

import org.apache.commons.lang3.LocaleUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;


public class MappingProperties {

    private static final MappingCache cache = new MappingCache();

    public MappingProperties(String user) {
        if (!cache.contains(user)) {
            cache.put(user, loadMappings(user));
        }
    }

    private static String getLanguageTag(File file) {
        return file.getName().split(".locale")[0];
    }

    private Map<Locale, Properties> loadMappings(String user) {
        File mappingFolder = new File(
                ApplicationProperties.getConfigurationDir()
                        + ApplicationProperties.getFileSeperator()
                        + user
                        + ApplicationProperties.getFileSeperator()
                        + ApplicationProperties.getMappingDir());

        Map<Locale, Properties> properties = new HashMap<>();

        if (mappingFolder.exists() && mappingFolder.isDirectory()) {
            final File[] files = mappingFolder.listFiles();
            Arrays.stream(files).forEach(file -> loadMappings(properties, file));
        }
        return properties;
    }

    private Map<Locale, Properties> loadMappings(Map<Locale, Properties> properties, File file) {
        try (InputStream in = new FileInputStream(file)) {
            Properties props = new Properties();
            props.load(new InputStreamReader(in, StandardCharsets.UTF_8));
            properties.put(LocaleUtils.toLocale(getLanguageTag(file)), props);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    public String get(String topic, String user, Locale locale) {
        return cache.get(user, locale).getProperty(topic, topic);
    }
}
