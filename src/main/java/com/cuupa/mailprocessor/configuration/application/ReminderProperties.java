package com.cuupa.mailprocessor.configuration.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ReminderProperties {

    private static final Map<String, Properties> cache = new HashMap<>();

    private final String user;

    public ReminderProperties(String user) {
        this.user = user;
        if (cache.containsKey(user)) {
            return;
        }
        File file = new File(ApplicationProperties.getConfigurationDir() +
                                     user
                                     + ApplicationProperties.getFileSeperator()
                                     + "reminder.properties");
        if (file.exists()) {
            try (InputStream in = new FileInputStream(file)) {
                Properties props = new Properties();
                props.load(in);
                cache.put(user, props);
            } catch (IOException e) {

            }
        }
    }

    public boolean hasReminder() {
        return cache.containsKey(user);
    }

    public String getUrlToCall() {
        return cache.get(user).getProperty("urlToCall", "");
    }

    public String getBotUsername() {
        return cache.get(user).getProperty("username");
    }

    public String getBotToken() {
        return cache.get(user).getProperty("botToken");
    }

    public String getChatId() {
        return cache.get(user).getProperty("chat_id");
    }
}
