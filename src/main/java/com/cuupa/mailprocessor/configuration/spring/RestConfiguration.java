package com.cuupa.mailprocessor.configuration.spring;

import com.cuupa.mailprocessor.services.extern.ExternConverterService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfiguration {

	@Bean
	public ExternConverterService externConverterService() {
		return new ExternConverterService();
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
