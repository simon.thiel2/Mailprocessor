package com.cuupa.mailprocessor.configuration.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({EmailDelegateConfiguration.class, ScanDelegateConfiguration.class})
public class DelegateConfiguration {
	
}
