package com.cuupa.mailprocessor.configuration.spring;

import com.cuupa.mailprocessor.delegates.scan.*;
import com.cuupa.mailprocessor.services.ReminderService;
import com.cuupa.mailprocessor.services.extern.ExternSemanticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ScanDelegateConfiguration {

    @Autowired
    private ReminderService reminderService;

    @Bean("plaintextDelegate")
    public PlaintextDelegate plaintextDelegate() {
        return new PlaintextDelegate();
    }

    @Bean("semanticDelegate")
    public SemanticDelegate semanticScanDelegate() {
        return new SemanticDelegate(externSemanticService());
    }

    @Bean
    public ExternSemanticService externSemanticService() {
        return new ExternSemanticService();
    }

    @Bean("archiveScanDelegate")
    public ArchiveScanDelegate archiveScanDelegate() {
        return new ArchiveScanDelegate();
    }

    @Bean("translateTopicDelegate")
    public TranslateTopicDelegate translateTopicDelegate() {
        return new TranslateTopicDelegate();
    }

    @Bean("dmnResultMapperDelegate")
    public DmnResultMapperDelegate dmnResultMapperDelegate() {
        return new DmnResultMapperDelegate();
    }

    @Bean("detectReminderDelegate")
	public DetectReminderDelegate detectReminderDelegate() {
    	return new DetectReminderDelegate();
	}

    @Bean("triggerReminderDelegate")
    public TriggerReminderDelegate triggerReminderDelegate() {
        return new TriggerReminderDelegate(reminderService);
    }
}
