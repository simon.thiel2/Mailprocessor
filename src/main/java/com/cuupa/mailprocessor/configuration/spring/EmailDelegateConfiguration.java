package com.cuupa.mailprocessor.configuration.spring;

import com.cuupa.mailprocessor.delegates.email.*;
import com.cuupa.mailprocessor.services.input.email.ImapMailService;
import com.cuupa.mailprocessor.services.input.email.MailService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailDelegateConfiguration {

    @Bean
    public ArchiveEmailDelegate archiveDelegate() {
        return new ArchiveEmailDelegate(mailService());
    }

    @Bean
    MailService mailService() {
        return new ImapMailService();
    }

    @Bean
    public EmailDelegate emailDelegate() {
        return new EmailDelegate();
    }

    @Bean
    public AttachmentDelegate attachmentDelegate() {
        return new AttachmentDelegate();
    }

    @Bean
    public DecryptorDelegate decryptorDelegate() {
        return new DecryptorDelegate();
    }

    @Bean
    public DownloadTypeDelegate downloadTypeDelegate() {
        return new DownloadTypeDelegate();
    }

    @Bean
    public DownloadTypeDelegate downloadChooser() {
        return new DownloadTypeDelegate();
    }
}
