package com.cuupa.mailprocessor.configuration.spring;

import com.cuupa.mailprocessor.MailProcessorCamundaProcessApplication;
import com.cuupa.mailprocessor.services.ProcessService;
import com.cuupa.mailprocessor.services.ReminderService;
import com.cuupa.mailprocessor.services.WorkerService;
import com.cuupa.mailprocessor.services.input.scan.ScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

@Configuration
@Import({DelegateConfiguration.class, RestConfiguration.class, CamundaConfiguration.class})
@EnableScheduling
@EnableAsync
public class ApplicationConfiguration {

    @Autowired
    private EmailDelegateConfiguration emailDelegateConfiguration;

    @Bean
    public MailProcessorCamundaProcessApplication mailProcessorProcessApplication() {
        return new MailProcessorCamundaProcessApplication();
    }

    @Bean
    public ProcessService processService() {
        return new ProcessService();
    }

    @Bean
    public TaskScheduler taskScheduler() {
        return new ConcurrentTaskScheduler();
    }

    @Bean
    public WorkerService workerService() {
        return new WorkerService(processService(), emailDelegateConfiguration.mailService(), scanService());
    }

    @Bean
    public ScanService scanService() {
        return new ScanService();
    }

    @Bean
    public ReminderService reminderService() {
        return new ReminderService();
    }
}
