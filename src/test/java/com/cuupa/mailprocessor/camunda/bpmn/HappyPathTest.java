package com.cuupa.mailprocessor.camunda.bpmn;

import com.cuupa.mailprocessor.process.MailProcessProperty;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.impl.test.PluggableProcessEngineTestCase;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.mock.Mocks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.assertThat;

/**
 * @author Simon Thiel (https://github.com/cuupa)
 */
public class HappyPathTest extends PluggableProcessEngineTestCase {

    private static final String BPMN_FILE = "mailprocessor.bpmn";

    private static final String EMPTY_DMN = "empty.dmn";

    private static final String text_found_gateway = "text_found_gateway";

    public void setup() {
        Mocks.register("plaintextDelegate", new JavaDelegate() {
            @Override
            public void execute(DelegateExecution delegateExecution) throws Exception {
                System.out.println("plaintextDelegate");
            }
        });

        Mocks.register("semanticDelegate", new JavaDelegate() {
            @Override
            public void execute(DelegateExecution delegateExecution) throws Exception {
                System.out.println("semanticDelegate");
            }
        });

        Mocks.register("archiveScanDelegate", new JavaDelegate() {
            @Override
            public void execute(DelegateExecution delegateExecution) throws Exception {
                System.out.println("archiveDelegate");
            }
        });
    }

    @Deployment(resources = {BPMN_FILE, EMPTY_DMN})
    public void testHappyPath() {
        setup();
        Map<String, Object> variables = new HashMap<>();
        variables.put(MailProcessProperty.PLAIN_TEXT.getName(), new ArrayList<String>());
        variables.put("user", "empty");
        ProcessInstance instance = runtimeService.startProcessInstanceByKey("mailprocessor", variables);

        assertThat(instance).hasPassed("plaintextDelegate").hasPassed(text_found_gateway).hasPassed("semanticDelegate");
    }
}
