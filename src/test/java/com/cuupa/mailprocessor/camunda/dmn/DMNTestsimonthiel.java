package com.cuupa.mailprocessor.camunda.dmn;

import org.camunda.bpm.dmn.engine.DmnDecisionTableResult;
import org.camunda.bpm.engine.impl.test.PluggableProcessEngineTestCase;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Simon Thiel (https://github.com/cuupa)
 */
public class DMNTestsimonthiel extends PluggableProcessEngineTestCase {

    public static final String SIMON_THIEL_DMN = "simon.thiel.dmn";

    public static final String TOPIC = "topic";

    public static final String SENDER = "sender";

    @Deployment(resources = SIMON_THIEL_DMN)
    public void testShouldBeUnbekannt() {
        // Set input variables
        VariableMap variables = Variables.createVariables()
                .putValue(TOPIC, null)
                .putValue("sender", "UNKNOWN");

        DmnDecisionTableResult results = processEngine.getDecisionService().evaluateDecisionTableByKey("simon.thiel", variables);

        // Check that one rule has matched
        assertThat(results).hasSize(1);
        assertThat(results.getFirstResult()).containsValue("/homes/simon.thiel/Drive/Meine Dokumente/Post/unbekannt/");

        variables = Variables.createVariables()
                .putValue("topic", "WARNING")
                .putValue("sender", "UNKNOWN");

        results = processEngine.getDecisionService().evaluateDecisionTableByKey("simon.thiel", variables);

        // Check that one rule has matched
        assertThat(results).hasSize(1);
        assertThat(results.getFirstResult()).containsValue("/homes/simon.thiel/Drive/Meine Dokumente/Post/unbekannt/");
    }


    @Deployment(resources = SIMON_THIEL_DMN)
    public void testShouldBeArag() {
        // Set input variables
        VariableMap variables = Variables.createVariables()
                .putValue(TOPIC, null)
                .putValue("sender", "ARAG Versicherungen");

        DmnDecisionTableResult results = processEngine.getDecisionService().evaluateDecisionTableByKey("simon.thiel", variables);

        // Check that one rule has matched
        assertThat(results).hasSize(1);
        assertThat(results.getFirstResult()).containsValue("/homes/simon.thiel/Drive/Meine Dokumente/Post/Versicherung/%sender%/%policynumber%/");

        variables = Variables.createVariables()
                .putValue("topic", "WARNING")
                .putValue("sender", "ARAG Versicherungen");

        results = processEngine.getDecisionService().evaluateDecisionTableByKey("simon.thiel", variables);

        // Check that one rule has matched
        assertThat(results).hasSize(1);
        assertThat(results.getFirstResult()).containsValue("/homes/simon.thiel/Drive/Meine Dokumente/Post/Versicherung/%sender%/%policynumber%/");
    }

    @Deployment(resources = SIMON_THIEL_DMN)
    public void testShouldBeProvinzial() {
        // Set input variables
        VariableMap variables = Variables.createVariables()
                .putValue(TOPIC, null)
                .putValue(SENDER, "Provinzial Rheinland Versicherung AG");

        DmnDecisionTableResult results = processEngine.getDecisionService().evaluateDecisionTableByKey("simon.thiel", variables);

        // Check that one rule has matched
        assertThat(results).hasSize(1);
        assertThat(results.getFirstResult()).containsValue("/homes/simon.thiel/Drive/Meine Dokumente/Post/Versicherung/%sender%/%policynumber%/");

        variables = Variables.createVariables()
                .putValue("topic", "WARNING")
                .putValue("sender", "Provinzial Rheinland Versicherung AG");

        results = processEngine.getDecisionService().evaluateDecisionTableByKey("simon.thiel", variables);

        // Check that one rule has matched
        assertThat(results).hasSize(1);
        assertThat(results.getFirstResult()).containsValue("/homes/simon.thiel/Drive/Meine Dokumente/Post/Versicherung/%sender%/%policynumber%/");
    }

    @Deployment(resources = SIMON_THIEL_DMN)
    public void testShouldBeOther() {
        // Set input variables
        VariableMap variables = Variables.createVariables()
                .putValue(TOPIC, null)
                .putValue(SENDER, "Teletubbies & Co KG");

        DmnDecisionTableResult results = processEngine.getDecisionService().evaluateDecisionTableByKey("simon.thiel", variables);

        // Check that one rule has matched
        assertThat(results).hasSize(1);
        assertThat(results.getFirstResult()).containsValue("/homes/simon.thiel/Drive/Meine Dokumente/Post/%sender%/");

        variables = Variables.createVariables()
                .putValue("topic", "WARNING")
                .putValue("sender", "Teletubbies & Co KG");

        results = processEngine.getDecisionService().evaluateDecisionTableByKey("simon.thiel", variables);

        // Check that one rule has matched
        assertThat(results).hasSize(1);
        assertThat(results.getFirstResult()).containsValue("/homes/simon.thiel/Drive/Meine Dokumente/Post/%sender%/");
    }
}
