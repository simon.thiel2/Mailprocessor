package com.cuupa.mailprocessor.service;

import com.cuupa.mailprocessor.services.ReminderDelegate;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.assertFalse;

public class ReminderServiceTest {

    private ReminderDelegate service;

	@Before
	public void setUp() {
        service = new ReminderDelegate();
	}

	@Test
	public void shouldReturnFalseForEmpty() {
		boolean reminder = service.isCandidateForReminder("");
		assertFalse(reminder);

		reminder = service.isCandidateForReminder(null);
		assertFalse(reminder);

		reminder = service.isCandidateForReminder(" ");
		assertFalse(reminder);
		
		reminder = service.isCandidateForReminder("test");
		assertFalse(reminder);
	}
	
	@Test(expected = DateTimeParseException.class)
	public void shouldFailParsing() {
		service.isCandidateForReminder("test.");
	}
	
	@Test
	public void shouldParseFilename() {
		service.isCandidateForReminder("31.12.2018");
	}
	
	@Test
	public void shouldNotBeCandidateForReminder() {
		String string = LocalDate.now().toString();
		System.out.println(string);
	}
}
