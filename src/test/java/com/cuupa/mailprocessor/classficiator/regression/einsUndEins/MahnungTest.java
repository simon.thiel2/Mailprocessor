package com.cuupa.mailprocessor.classficiator.regression.einsUndEins;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.cuupa.mailprocessor.configuration.application.ApplicationProperties;
import com.cuupa.mailprocessor.delegates.scan.PlaintextDelegate;
import com.cuupa.mailprocessor.tos.mail.PDF;

public class MahnungTest {

	private List<List<String>> list = new ArrayList<>();
	
	private static final String sender = "1&1 Internet AG";
	
	private final PlaintextDelegate plaintext = new PlaintextDelegate();
	
	private final RestTemplate template = new RestTemplate();
	
	@Before
	public void lese() {
		File folder = new File("D:\\Users\\Simon\\Desktop\\Testdaten\\" + sender + "\\Mahnung");
		
		File[] files = folder.listFiles();
		
		for (File file : files) {
			try(InputStream in = new FileInputStream(file)) {
				PDF pdf = new PDF();
				pdf.setContent(IOUtils.toByteArray(in));
				pdf.setFileName(file.getName());
				pdf.setUser("simon.thiel");
				
//				pdf = (PDF) plaintext.execute(pdf);
				
				list.add(pdf.getPlainText());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	@Test
	public void shouldBeMahnung() {
		for (List<String> text : list) {
			StringBuilder sb = new StringBuilder();
			for (String string : text) {
				sb.append(string);
			}
			ResponseEntity<String> response = template.postForEntity("http://localhost:8080/classificator/classifyText", sb.toString(), String.class);
			
			System.out.println(response.getBody());
			
			assertTrue(response.getBody().contains("{\"topicName\":\"WARNING\","));
		}
	}
}
