package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.process.MailInstanceHandler;
import com.cuupa.mailprocessor.services.ReminderService;
import com.cuupa.mailprocessor.tos.mail.PDF;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.impl.pvm.runtime.ExecutionImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TriggerReminderDelegateTest {

    private TriggerReminderDelegate unitToTest;

    private ReminderService reminderService;

    @Before
    public void setUp() {
        reminderService = new ReminderService();
        unitToTest = new TriggerReminderDelegate(reminderService);
    }

    @Test
    public void shouldTriggerReminder() throws Exception {
        List<String> reminderDates = new ArrayList<>();
        reminderDates.add("02.02.2020");

        PDF pdf = new PDF();
        pdf.setUser("testuser");
        pdf.setFrom("Company");
        pdf.setFileName("test.pdf");
        pdf.addTopic("TestTopic");
        pdf.addMetaData(new ArrayList<>());

        MailInstanceHandler handler = new MailInstanceHandler(pdf);
        handler.getVariables().put("reminderDates", reminderDates);

        DelegateExecution execution = new ExecutionImpl();
        execution.setVariablesLocal(handler.getVariables());

        unitToTest.execute(execution);
    }


}