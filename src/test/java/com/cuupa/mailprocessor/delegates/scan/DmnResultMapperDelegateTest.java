package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.process.MailInstanceHandler;
import com.cuupa.mailprocessor.tos.mail.PDF;
import com.cuupa.mailprocessor.tos.semantic.Metadata;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.impl.pvm.runtime.ExecutionImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DmnResultMapperDelegateTest {

    private DmnResultMapperDelegate unitToTest;

    @BeforeEach
    void setUp() {
        unitToTest = new DmnResultMapperDelegate();
    }

    @Test
    void execute() throws Exception {
        PDF pdf = new PDF();
        pdf.setUser("testuser");
        pdf.setFrom("Company");
        pdf.setFileName("test.pdf");
        pdf.addTopic("TestTopic");
        List<Metadata> list = new ArrayList<>();
        Metadata policyNumber = new Metadata("policynumber", "1234567890");
        list.add(policyNumber);
        Metadata sender = new Metadata("sender", "Company");
        list.add(sender);
        pdf.addMetaData(list);

        MailInstanceHandler handler = new MailInstanceHandler(pdf);
        Map<String, Object> dmnResult = new HashMap<>();
        dmnResult.put("hasReminder", false);
        dmnResult.put("path_to_save",
                      "\"/homes/simon.thiel/Drive/Meine Dokumente/Post/Versicherung/%sender%/%policynumber%/\"");
        handler.getVariables().put("dmn_result", dmnResult);

        DelegateExecution execution = new ExecutionImpl();
        execution.setVariablesLocal(handler.getVariables());

        unitToTest.execute(execution);

        assertEquals("\"/homes/simon.thiel/Drive/Meine Dokumente/Post/Versicherung/Company/1234567890/\"",
                     execution.getVariable("path_to_save"));

    }
}