package com.cuupa.mailprocessor.delegates.scan;

import com.cuupa.mailprocessor.services.archiver.FileProtocol;
import com.cuupa.mailprocessor.services.archiver.FileProtocolFactory;
import com.cuupa.mailprocessor.services.archiver.LocalArchiver;
import com.cuupa.mailprocessor.services.archiver.WebDav;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class FileProtocolFactoryTest {

    @Test
    public void shouldGetWebdavArchiver() {
        FileProtocol fileProtocol = FileProtocolFactory.get("https://webdav.address.com", "user", "password");
        assertTrue(fileProtocol instanceof WebDav);
    }

    @Test
    public void shouldBeLocalWindowsArchiverC() {
        FileProtocol fileProtocol = FileProtocolFactory.get("C:\\path\\to\\docs", null, null);
        assertTrue(fileProtocol instanceof LocalArchiver);
    }

    @Test
    public void shouldBeLocalWindowsArchiverX() {
        FileProtocol fileProtocol = FileProtocolFactory.get("X:\\path\\to\\docs", null, null);
        assertTrue(fileProtocol instanceof LocalArchiver);
    }

    @Test
    public void shouldBeLocalWindowsArchiverWithWrongSeperator() {
        FileProtocol fileProtocol = FileProtocolFactory.get("C:/path/to\\docs", null, null);
        assertTrue(fileProtocol instanceof LocalArchiver);
    }

    @Test
    public void shoudBeLocalUnixArchiver() {
        FileProtocol fileProtocol = FileProtocolFactory.get("/usr/home", null, null);
        assertTrue(fileProtocol instanceof LocalArchiver);
    }
}